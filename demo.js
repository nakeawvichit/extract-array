let input = {
    "products": [
        {
            "variants": ["souffle", "cake", "ice - cream"]
        },
        {
            "variants": ["cherry", "choco"]
        },
        {
            "variants": ["XL"]
        }
    ]
}

let output = [
    "souffle cherry XL",
    "cake cherry XL",
    "ice - cream cherry XL",

    "souffle choco XL",
    "cake choco XL",
    "ice - cream choco XL",
]

let arr = []
let temp = ""
let totalLoop = 1
let tempArr = [] // row level
let myLength = []

for (let i = 0; i < input.products.length; i++) {
    tempArr.push(0)
    myLength.push(input.products[i].variants.length - 1)
    totalLoop *= input.products[i].variants.length
}

for (let loop = 0; loop < totalLoop; loop++) {
    console.log(tempArr)
    for (let i = 0; i < input.products.length; i++) { // rows
        temp += extractArr(input.products[i].variants, tempArr[i]) + " "
    }
    arr.push(temp.trim())
    temp = ""
    for (let x = 0; x < tempArr.length; x++) {
        if (tempArr[x] < myLength[x]) {
            tempArr[x] += 1
            break
        } else {
            tempArr[x] = 0
        }
        if (x + 1 <= tempArr.length &&
            (tempArr[x] + 1) == myLength[x] &&
            (tempArr[x + 1] + 1) < myLength[x + 1]) {
            tempArr[x + 1] += 1
            tempArr[x] = 0
        }
    }
}

function extractArr(arr, index) {
    return arr[index]
}
console.log('######################')
console.log(arr)
